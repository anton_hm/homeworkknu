using System.Text.Json;
using System.Text.Json.Serialization;
using System.Text.Json;
using System.IO;
using System.Net.Http.Json;
using Newtonsoft.Json;
using System.Text.Json.Nodes;
using JsonSerializer = System.Text.Json.JsonSerializer;

namespace Homeworkjson
{
    public partial class Welcome
    {
        public long PublishingHouseId { get; set; }
        public string Title { get; set; }
        public PublishingHouse PublishingHouse { get; set; }
    }

    public partial class PublishingHouse
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string Adress { get; set; }
    }
    public class Program
    {
        static async Task Main()
        {
            string fileName = "task.json";
            string jsonString = File.ReadAllText(fileName);
            using (FileStream fs = new FileStream(fileName, FileMode.Open, FileAccess.Read))
            {
                var result = await JsonSerializer.DeserializeAsync<List<Welcome>>(fs);
                foreach (var house in result)
                {
                    Console.WriteLine($"PhId: {house.PublishingHouseId} Title: {house.Title} Name: {house.PublishingHouse.Name} " +
                        $"Adress: {house.PublishingHouse.Adress} Id: {house.PublishingHouse.Id}");
                }
            }
        }
    }
}
